%% Definition and selection of an activation function
% Returns:
%   AF_handle: the handle of the activation function
%   AF_local_bounds: the handle taking input bounds and 
%       returning local bounds of the activation function
%   AF_derivative: the handle of the activation function derivative
%   AF_derivative_local_bounds: the handle taking input bounds and 
%       returning local bounds of the derivative of the activation function
%   [AF_low,AF_up]: global/worst-case bounds of the activation function
%   [AF_derivative_low,AF_derivative_up]: global/worst-case bounds of the 
%       derivative of the activation function
function [AF_handle,AF_local_bounds,AF_derivative,AF_derivative_local_bounds,AF_low,AF_up,AF_derivative_low,AF_derivative_up] = Activation_function_select(index)

% AF_IDENTITY = 1;
% AF_HEAVISIDE = 2;
% AF_SIGMOID = 3;
% AF_TANH = 4;
% AF_RELU = 5;
% AF_PRELU = 6;
% AF_SOFTPLUS = 7;
% AF_ELU = 8;
% AF_GELU = 9;        % Not monotone
% AF_SILU = 10;       % Not monotone
% AF_GAUSSIAN = 11;   % Not monotone

switch index
    case 1
        % Identity
        AF_handle = @(x) x;
        AF_low = -Inf;
        AF_up = Inf;
        AF_local_bounds = @(x_low,x_up) [AF_handle(x_low),AF_handle(x_up)];
        
        AF_derivative = @(x) 1;
        AF_derivative_low = 1;
        AF_derivative_up = 1;
        AF_derivative_local_bounds = @(x_low,x_up) [AF_derivative(x_low),AF_derivative(x_up)];
    case 2
        % Binary step
        AF_handle = @(x) (x>=0)*1;
        AF_low = 0;
        AF_up = 1;
        AF_local_bounds = @(x_low,x_up) [AF_handle(x_low),AF_handle(x_up)];
        
        AF_derivative = NaN;
        AF_derivative_low = 0;
        AF_derivative_up = Inf;     % reached for x=0
        AF_derivative_local_bounds = @Heaviside_derivative_local_bounds;
    case 3
        % Sigmoid
        AF_handle = @(x) 1/(1+exp(-x));
        AF_low = 0;
        AF_up = 1;
        AF_local_bounds = @(x_low,x_up) [AF_handle(x_low),AF_handle(x_up)];
        
        AF_derivative = @(x) exp(-x)/(1+exp(-x))^2;
        AF_derivative_low = 0;
        AF_derivative_up = 0.25;    % reached for x=0
        AF_derivative_local_bounds = @(x_low,x_up) [min(AF_derivative(x_low),AF_derivative(x_up)), ...
                                                    (x_low<=0 && x_up>=0)*AF_derivative_up + (x_low>0 || x_up<0)*max(AF_derivative(x_low),AF_derivative(x_up))];
    case 4
        % Hyperbolic tangent
        AF_handle = @tanh;
        AF_low = -1;
        AF_up = 1;
        AF_local_bounds = @(x_low,x_up) [AF_handle(x_low),AF_handle(x_up)];
        
        AF_derivative = @(x) 1-AF_handle(x)^2;
        AF_derivative_low = 0;
        AF_derivative_up = 1;       % reached for x=0
        AF_derivative_local_bounds = @(x_low,x_up) [min(AF_derivative(x_low),AF_derivative(x_up)), ...
                                                    (x_low<=0 && x_up>=0)*AF_derivative_up + (x_low>0 || x_up<0)*max(AF_derivative(x_low),AF_derivative(x_up))];
    case 5
        % ReLU
        AF_handle = @(x) max(0,x);
        AF_low = 0;
        AF_up = Inf;
        AF_local_bounds = @(x_low,x_up) [AF_handle(x_low),AF_handle(x_up)];
        
        AF_derivative = @(x) (x>=0)*1;
        AF_derivative_low = 0;
        AF_derivative_up = 1;
        AF_derivative_local_bounds = @(x_low,x_up) [AF_derivative(x_low),AF_derivative(x_up)];
    case 6
        % Parametrized ReLU
        a = 0.01;
        AF_handle = @(x) max(a*x,x);
        AF_low = -Inf;
        AF_up = Inf;
        AF_local_bounds = @(x_low,x_up) [AF_handle(x_low),AF_handle(x_up)];
        
        AF_derivative = @(x) (x>=0)*1 + (x<0)*a;
        AF_derivative_low = a;
        AF_derivative_up = 1;
        AF_derivative_local_bounds = @(x_low,x_up) [AF_derivative(x_low),AF_derivative(x_up)];
    case 7
        % Softplus
        AF_handle = @(x) log(1+exp(x));
        AF_low = 0;
        AF_up = Inf;
        AF_local_bounds = @(x_low,x_up) [AF_handle(x_low),AF_handle(x_up)];
        
        AF_derivative = @(x) 1/(1+exp(-x));
        AF_derivative_low = 0;
        AF_derivative_up = 1;
        AF_derivative_local_bounds = @(x_low,x_up) [AF_derivative(x_low),AF_derivative(x_up)];
    case 8
        % Exponential linear unit
        a = 1;
        AF_handle = @(x) ELU_handle(x,a);
        AF_low = -a;
        AF_up = Inf;
        AF_local_bounds = @(x_low,x_up) [AF_handle(x_low),AF_handle(x_up)];
        
        AF_derivative = @(x) ELU_derivative(x,a);
        AF_derivative_low = 0;
        AF_derivative_up = 1;
        AF_derivative_local_bounds = @(x_low,x_up) [AF_derivative(x_low),AF_derivative(x_up)];
    case 9
        % GELU
        phi = @(x) (1+erf(x/sqrt(2)))/2;
        AF_handle = @(x) x*phi(x);
        AF_low = -0.17;             % reached for x=-0.7518
        x_argmin = -0.7518;
        AF_up = Inf;
        AF_local_bounds = @(x_low,x_up) Automatic_local_bound(x_low,x_up,AF_handle,AF_low,x_argmin,AF_up,Inf);
                                     
        AF_derivative = @(x) phi(x)+x*exp(-x^2/2)/sqrt(2*pi);
        AF_derivative_low = -0.129; % reached for x=-1.4142
        x_argmin = -1.4142;
        AF_derivative_up = 1.129;   % reached for x=1.4142
        x_argmax = 1.4142;
        AF_derivative_local_bounds = @(x_low,x_up) [(x_low<=x_argmin && x_up>=x_argmin)*AF_derivative_low + (x_low>x_argmin || x_up<x_argmin)*min(AF_derivative(x_low),AF_derivative(x_up)), ...
                                                    (x_low<=x_argmax && x_up>=x_argmax)*AF_derivative_up + (x_low>x_argmax || x_up<x_argmax)*max(AF_derivative(x_low),AF_derivative(x_up))];
    case 10
        % SiLU
        AF_handle = @(x) x/(1+exp(-x));
        AF_low = -0.278;            % reached for x=-1.2785
        x_argmin = -1.2785;
        AF_up = Inf;       
        AF_local_bounds = @(x_low,x_up) Automatic_local_bound(x_low,x_up,AF_handle,AF_low,x_argmin,AF_up,Inf);
        
        AF_derivative = @(x) (1+exp(-x)+x*exp(-x))/(1+exp(-x))^2;
        AF_derivative_low = -0.1;   % reached for x=-2.3994
        x_argmin = -2.3994;
        AF_derivative_up = 1.1;     % reached for x=2.3994
        x_argmax = 2.3994;                                  
        AF_derivative_local_bounds = @(x_low,x_up) Automatic_local_bound(x_low,x_up,AF_derivative,AF_derivative_low,x_argmin,AF_derivative_up,x_argmax);
        
    case 11
        % Gaussian
        AF_handle = @(x) exp(-x^2);
        AF_low = 0;
        AF_up = 1;                  % reached for x=0
        AF_local_bounds = @(x_low,x_up) [min(AF_handle(x_low),AF_handle(x_up)), ...
                                         (x_low<=0 && x_up>=0)*AF_up + (x_low>0 || x_up<0)*max(AF_handle(x_low),AF_handle(x_up))];
        
        AF_derivative = @(x) -2*x*exp(-x^2);
        AF_derivative_low = -0.86;  % reached for x=0.7071
        x_argmin = 0.7071;
        AF_derivative_up = 0.86;    % reached for x=-0.7071
        x_argmax = -0.7071;
        AF_derivative_local_bounds = @(x_low,x_up) [(x_low<=x_argmin && x_up>=x_argmin)*AF_derivative_low + (x_low>x_argmin || x_up<x_argmin)*min(AF_derivative(x_low),AF_derivative(x_up)), ...
                                                    (x_low<=x_argmax && x_up>=x_argmax)*AF_derivative_up + (x_low>x_argmax || x_up<x_argmax)*max(AF_derivative(x_low),AF_derivative(x_up))];                                               
                                                
    case 12
        % Template for new user-provided activation function
        % whose function and derivative satisfy the conditions from 
        % Section 2.3 of the HSCC22 submission
        
%         % User-defined activation function
%         AF_handle = @(x) ...;   % handle of the activation function
%         AF_low = ...;           % Global min of the activation function (can be Inf)
%         x_argmin = ...;         % Input argument reaching the global min (can be Inf)
%         AF_up = ...;            % Global max of the activation function (can be Inf)
%         x_argmax = ...;         % Input argument reaching the global max (can be Inf)
%         
%         % Automatically created function getting local bounds of the
%         % activation function based on pre-activation bounds
%         AF_local_bounds = @(x_low,x_up) Automatic_local_bound(x_low,x_up,AF_handle,AF_low,x_argmin,AF_up,x_argmax);
%                                      
%         % User-defined derivative of the activation function
%         AF_derivative = @(x) ...;   % handle of the derivative
%         AF_derivative_low = ...;    % Global min of the derivative (can be Inf)
%         x_argmin = ...;             % Input argument reaching the global min (can be Inf)
%         AF_derivative_up = ...;     % Global max of the derivative (can be Inf)
%         x_argmax = ...;             % Input argument reaching the global max (can be Inf)
%         
%         % Automatically created function getting local bounds of the
%         % derivative based on pre-activation bounds
%         AF_derivative_local_bounds = @(x_low,x_up) Automatic_local_bound(x_low,x_up,AF_derivative,AF_derivative_low,x_argmin,AF_derivative_up,x_argmax);
end
end

%% Heaviside: local bounds on the derivative
function AF_derivative_local_bounds = Heaviside_derivative_local_bounds(x_low,x_up)
if (x_low<=0 && x_up>=0)
    AF_derivative_local_bounds = [0,Inf];
else
    AF_derivative_local_bounds = [0,0];
end
end

%% ELU: activation function handle
function AF_handle = ELU_handle(x,a)
if x>=0
    AF_handle = x;
else
    AF_handle = a*(exp(x)-1);
end
end

%% ELU: derivative handle
function AF_derivative = ELU_derivative(x,a)
if x>=0
    AF_derivative = 1;
else
    AF_derivative = a*exp(x);
end
end
         
%% Automatic generation of a function handle returning local bounds of a function
% As in Proposition 2.2 (in Section 2.3) of the HSCC'22 submission
% "Reachability analysis of neural networks using mixed monotonicity"
% Inputs:
%   [x_low,x_up]: pre-activation bounds
%   f_handle: function handle (activation function or its derivative)
%   f_min,f_max: global extrema of f_handle
%   f_argmin,f_argmax: input arguments corresponding to the global extrema
% Outputs:
%   local_bound_handle: function handle
%       taking pre-activation bounds [x_low,x_up] as input argument
%       and returning the corresponding local bounds of f_handle
function local_bound_handle = Automatic_local_bound(x_low,x_up,f_handle,f_min,f_argmin,f_max,f_argmax)

% Lower bound
if (x_low<=f_argmin && x_up>=f_argmin)
    % Global minimum is reached
    local_bound_handle(1) = f_min;
else
    local_bound_handle(1) = min(f_handle(x_low),f_handle(x_up));
end

% Upper bound
if (x_low<=f_argmax && x_up>=f_argmax)
    % Global maximum is reached
    local_bound_handle(2) = f_max;
else
    local_bound_handle(2) = max(f_handle(x_low),f_handle(x_up));
end
end
