%% Mixed-monotonicity reachability analysis for algebraic functions
% based on bounds [J_low,J_up] on the Jacobian matrix of the function and
% the input bounds [input_low,input_up] of the system.
% Returns interval bounds [output_low,output_up] on the system output
function [output_low,output_up] = Algebraic_MM(input_low,input_up,J_low,J_up,system_handle)

[n_output,n_input] = size(J_low);

% Initialization of the auxiliary variables used in the over-approximation
xi_low = zeros(n_output,n_input);                % Inputs (one for each output dimension) used as lower bounds
xi_up = zeros(n_output,n_input);                 % Inputs (one for each output dimension) used as upper bounds
compensation = zeros(n_output,n_input);          % Vectors (one for each output dimension) used to compensate non sign-stable Jacobian

% Lower and upper bounds of the over-approximation 
output_low = zeros(n_output,1);
output_up = zeros(n_output,1);

% Over-approximation loop
% The reachable set is over-approximated independently for each output dimension 
for i = 1:n_output
    
    % Define the input auxiliary variables based on the Jacobian
    for j = 1:n_input
        % Center of Jacobian interval is positive 
        if (J_low(i,j)+J_up(i,j))/2 >= 0     
            xi_low(i,j) = input_low(j);
            xi_up(i,j) = input_up(j);
            if J_low(i,j) < 0      % Interval not fully positive
                compensation(i,j) = J_low(i,j);
            end
        % Center of Jacobian interval is negative 
        else                
            xi_low(i,j) = input_up(j);
            xi_up(i,j) = input_low(j);
            if J_up(i,j) > 0      % Interval not fully negative
                compensation(i,j) = J_up(i,j);
            end
        end
    end
    
    % Output from input xi_low(i,:)'
    xi_low_output = system_handle(xi_low(i,:)');
    % Output from input xi_up(i,:)'
    xi_up_output = system_handle(xi_up(i,:)');
    
    % Over-approximation on dimension i
    output_low(i) = xi_low_output(i)-compensation(i,:)*(xi_low(i,:)-xi_up(i,:))';
    output_up(i) = xi_up_output(i)+compensation(i,:)*(xi_low(i,:)-xi_up(i,:))';
end
