%% Neural-network over-approximation using symbolic interval propagation
% From CROWN paper: Huan Zhang, Tsui-Wei Weng, Pin-Yu Chen, Cho-Jui Hsieh,
% Luca Daniel. "Efficient Neural Network Robustness Certification with 
% General Activation Functions". Advances in Neural Information Processing 
% Systems, 2018.

% This approach is restricted to activation functions for which we can
% provide linear relaxation bounds in the form a*(x+b)
% Currently implemented: Identity, ReLU, tanh, sigmoid, softplus, ELU

% This method uses a backward propagation of linear bounds on the network.
% Since the backward propagation of the linear relaxations of activation
% functions rely on the knowledge of concretized values of the
% pre-activation bounds, the backward algorithm actually needs to be
% iteratively applied to each partial networks starting at the first layer,
% in order to compute these pre-activation bounds.

% Inputs
%   weights: cell array of the (hidden_layers+1) weight matrices
%   biases: cell array of the (hidden_layers+1) bias vectors
%   Activation_function_choise: index of the chosen activation function
%   input_low: lower bound of the uncertainty range of the input vector
%   input_up: upper bound of the uncertainty range of the input vector

% Outputs
%   [x_low,x_up]: bounds on the output range of the network

function [x_low,x_up] = NN_OA_CROWN(weights,biases,Activation_function_choise,input_low,input_up)
n_input = length(input_low);
max_layer_size = 0;
for i=1:length(biases)
    max_layer_size = max(max_layer_size,length(biases{i}));
end

% Number of iterations to find near-optimal relaxations of S-shaped AF
n_iter = 2;

%% Activation function index definitions
AF_IDENTITY = 1;
AF_SIGMOID = 3;
AF_TANH = 4;
AF_RELU = 5;
AF_SOFTPLUS = 7;
AF_ELU = 8;

%% Extract activation function informations
assert(ismember(Activation_function_choise,[AF_IDENTITY,AF_SIGMOID,AF_TANH,AF_RELU,AF_SOFTPLUS,AF_ELU]),'Activation function not supported by the method with symbolic intervals');
[AF_handle,AF_local_bounds,AF_derivative,~,~,~,~,~] = Activation_function_select(Activation_function_choise);

%% Initialization
% Cells for the lower and upper pre-activation bounds
PreActivation_low = cell(length(weights),1);
PreActivation_up = cell(length(weights),1);

% 4D matrix for the activation function relaxations
%   dimension 1: layer index
%   dimension 2: node index
%   dimension 3: 1 for lower bound, 2 for upper bound
%   dimension 4: 1 for slope, 2 for offset
%   (in the format: slope*(x+offset))
AFrelaxations = zeros(length(weights),max_layer_size,2,2);

%% Iterative call of the backward propagation algorithm on the growing partial networks
for l = 1:length(weights)   
    %% Compute pre-activation bounds of layer l
    [PreActivation_low{l},PreActivation_up{l}] = BackwardPropagation(l,AFrelaxations,weights,biases,input_low,input_up);
    
    %% Compute linear relaxations of the activation function for layer l
    % Note that linear relaxations are in the format slope*(x+offset)
    % which is different to slope*x+offset that we used in other files
    
    % Loop on output nodes of this layer
    for i = 1:size(weights{l},1)
        if PreActivation_up{l}(i) == PreActivation_low{l}(i)
            % Skip if bounds are equal
            AFrelaxations(l,i,1,2) = AF_handle(PreActivation_up{l}(i));
            AFrelaxations(l,i,2,2) = AFrelaxations(l,i,1,2);
        else
            % Otherwise, the relaxations depends on the activation type
            switch Activation_function_choise
                case AF_IDENTITY
                    % No nonlinearity in this AF, so no need for relaxation
                    AFrelaxations(l,i,1,1) = 1;
                    AFrelaxations(l,i,2,1) = 1;

                case AF_SIGMOID
                    % Preliminary computations
                    intercept_slope = (AF_handle(PreActivation_up{l}(i))-AF_handle(PreActivation_low{l}(i)))/(PreActivation_up{l}(i)-PreActivation_low{l}(i));
                    intercept_offset = AF_handle(PreActivation_low{l}(i))/intercept_slope - PreActivation_low{l}(i);
                    tangent_slope = AF_derivative((PreActivation_low{l}(i)+PreActivation_up{l}(i))/2);
                    tangent_offset = AF_handle((PreActivation_low{l}(i)+PreActivation_up{l}(i))/2)/tangent_slope - (PreActivation_low{l}(i)+PreActivation_up{l}(i))/2;
                    
                    if PreActivation_low{l}(i) >= 0
                        % Lower relaxation
                        AFrelaxations(l,i,1,1) = intercept_slope;
                        AFrelaxations(l,i,1,2) = intercept_offset;
                        % Upper relaxation
                        AFrelaxations(l,i,2,1) = tangent_slope;
                        AFrelaxations(l,i,2,2) = tangent_offset;
                    elseif PreActivation_up{l}(i) <= 0
                        % Lower relaxation
                        AFrelaxations(l,i,1,1) = tangent_slope;
                        AFrelaxations(l,i,1,2) = tangent_offset;
                        % Upper relaxation
                        AFrelaxations(l,i,2,1) = intercept_slope;
                        AFrelaxations(l,i,2,2) = intercept_offset;
                    else
                        % Lower relaxation
                        x_iter = PreActivation_low{l}(i);
                        for optim_iterations = 1:n_iter
                            temp = sqrt(1-4*(AF_handle(x_iter)-AF_handle(PreActivation_up{l}(i)))/(x_iter-PreActivation_up{l}(i)))/2;
                            x_iter = -log(1/(0.5-temp)-1);
                        end
                        AFrelaxations(l,i,1,1) = AF_derivative(x_iter);
                        AFrelaxations(l,i,1,2) = AF_handle(PreActivation_up{l}(i))/AFrelaxations(l,i,1,1) - PreActivation_up{l}(i);

                        % Upper relaxation
                        x_iter = PreActivation_up{l}(i);
                        for optim_iterations = 1:n_iter
                            temp = sqrt(1-4*(AF_handle(x_iter)-AF_handle(PreActivation_low{l}(i)))/(x_iter-PreActivation_low{l}(i)))/2;
                            x_iter = -log(1/(0.5+temp)-1);
                        end
                        AFrelaxations(l,i,2,1) = AF_derivative(x_iter);
                        AFrelaxations(l,i,2,2) = AF_handle(PreActivation_low{l}(i))/AFrelaxations(l,i,2,1) - PreActivation_low{l}(i);
                    end

                case AF_TANH
                    % Preliminary computations
                    intercept_slope = (AF_handle(PreActivation_up{l}(i))-AF_handle(PreActivation_low{l}(i)))/(PreActivation_up{l}(i)-PreActivation_low{l}(i));
                    intercept_offset = AF_handle(PreActivation_low{l}(i))/intercept_slope - PreActivation_low{l}(i);
                    tangent_slope = AF_derivative((PreActivation_low{l}(i)+PreActivation_up{l}(i))/2);
                    tangent_offset = AF_handle((PreActivation_low{l}(i)+PreActivation_up{l}(i))/2)/tangent_slope - (PreActivation_low{l}(i)+PreActivation_up{l}(i))/2;
                    
                    if PreActivation_low{l}(i) >= 0
                        % Lower relaxation
                        AFrelaxations(l,i,1,1) = intercept_slope;
                        AFrelaxations(l,i,1,2) = intercept_offset;
                        % Upper relaxation
                        AFrelaxations(l,i,2,1) = tangent_slope;
                        AFrelaxations(l,i,2,2) = tangent_offset;
                    elseif PreActivation_up{l}(i) <= 0
                        % Lower relaxation
                        AFrelaxations(l,i,1,1) = tangent_slope;
                        AFrelaxations(l,i,1,2) = tangent_offset;
                        % Upper relaxation
                        AFrelaxations(l,i,2,1) = intercept_slope;
                        AFrelaxations(l,i,2,2) = intercept_offset;
                    else
                        % Lower relaxation
                        x_iter = PreActivation_low{l}(i);
                        for optim_iterations = 1:n_iter
                            temp = sqrt(1-(AF_handle(x_iter)-AF_handle(PreActivation_up{l}(i)))/(x_iter-PreActivation_up{l}(i)));
                            x_iter = log((1-temp)/(1+temp))/2;
                        end
                        AFrelaxations(l,i,1,1) = AF_derivative(x_iter);
                        AFrelaxations(l,i,1,2) = AF_handle(PreActivation_up{l}(i))/AFrelaxations(l,i,1,1) - PreActivation_up{l}(i);

                        % Upper relaxation
                        x_iter = PreActivation_up{l}(i);
                        for optim_iterations = 1:n_iter
                            temp = sqrt(1-(AF_handle(x_iter)-AF_handle(PreActivation_low{l}(i)))/(x_iter-PreActivation_low{l}(i)));
                            x_iter = log((1+temp)/(1-temp))/2;
                        end
                        AFrelaxations(l,i,2,1) = AF_derivative(x_iter);
                        AFrelaxations(l,i,2,2) = AF_handle(PreActivation_low{l}(i))/AFrelaxations(l,i,2,1) - PreActivation_low{l}(i);
                    end
                    
                case AF_RELU
                    if PreActivation_low{l}(i) >= 0
                        AFrelaxations(l,i,1,1) = 1;
                        AFrelaxations(l,i,2,1) = 1;
                    elseif PreActivation_up{l}(i) > 0
                        AFrelaxations(l,i,1,1) = PreActivation_up{l}(i)/(PreActivation_up{l}(i)-PreActivation_low{l}(i));
                        AFrelaxations(l,i,2,1) = AFrelaxations(l,i,1,1);
                        AFrelaxations(l,i,2,2) = -PreActivation_low{l}(i);
                    end  
                    
                case AF_SOFTPLUS
                    % Preliminary computations
                    intercept_slope = (AF_handle(PreActivation_up{l}(i))-AF_handle(PreActivation_low{l}(i)))/(PreActivation_up{l}(i)-PreActivation_low{l}(i));
                    intercept_offset = AF_handle(PreActivation_low{l}(i))/intercept_slope - PreActivation_low{l}(i);
                    tangent_slope = AF_derivative((PreActivation_low{l}(i)+PreActivation_up{l}(i))/2);
                    tangent_offset = AF_handle((PreActivation_low{l}(i)+PreActivation_up{l}(i))/2)/tangent_slope - (PreActivation_low{l}(i)+PreActivation_up{l}(i))/2;

                    % Lower relaxation
                    AFrelaxations(l,i,1,1) = tangent_slope;
                    AFrelaxations(l,i,1,2) = tangent_offset;
                    % Upper relaxation
                    AFrelaxations(l,i,2,1) = intercept_slope;
                    AFrelaxations(l,i,2,2) = intercept_offset;
                    
                case AF_ELU
                    % Preliminary computations
                    intercept_slope = (AF_handle(PreActivation_up{l}(i))-AF_handle(PreActivation_low{l}(i)))/(PreActivation_up{l}(i)-PreActivation_low{l}(i));
                    intercept_offset = AF_handle(PreActivation_low{l}(i))/intercept_slope - PreActivation_low{l}(i);
                    tangent_slope = AF_derivative((PreActivation_low{l}(i)+PreActivation_up{l}(i))/2);
                    tangent_offset = AF_handle((PreActivation_low{l}(i)+PreActivation_up{l}(i))/2)/tangent_slope - (PreActivation_low{l}(i)+PreActivation_up{l}(i))/2;

                    % Lower relaxation
                    AFrelaxations(l,i,1,1) = tangent_slope;
                    AFrelaxations(l,i,1,2) = tangent_offset;
                    % Upper relaxation
                    AFrelaxations(l,i,2,1) = intercept_slope;
                    AFrelaxations(l,i,2,2) = intercept_offset;

            end
        end
    end
end

%% Propagation of the pre-activation bounds for the last layer through the last activation function
% Use simple forward propagation through AF_handle, as I am not sure how to
% exactly modify the backward propagation algorithm to take into account
% finishing on an activation function (instead of an affine transformation)
x_low = NaN(size(weights{l},1),1);
x_up = NaN(size(weights{l},1),1);
for i = 1:size(weights{l},1)
    AF_bounds = AF_local_bounds(PreActivation_low{end}(i),PreActivation_up{end}(i));
    x_low(i) = AF_bounds(1);
    x_up(i) = AF_bounds(2);
end

end


%% Backward propagation algorithm
% This function returns the concrete pre-activation bounds of output_layer,
% by applying the backward propagation algorithm on the partial network
% starting from layer 1 and ending on output_layer (without the last
% activation function)

% List of inputs:
%   output_layer: last layer of the considered partial network
%   AFrelaxations: 4D matrix for the activation function relaxations
%       dimension 1: layer index
%       dimension 2: node index
%       dimension 3: 1 for lower bound, 2 for upper bound
%       dimension 4: 1 for slope, 2 for offset
%   weights: cell array of the (hidden_layers+1) weight matrices
%   biases: cell array of the (hidden_layers+1) bias vectors
%   input_low: lower bound of the uncertainty range of the input vector
%   input_up: upper bound of the uncertainty range of the input vector

% Output:
%   [x_low,x_up]: pre-activation bounds of layer output_layer

function [x_low,x_up] = BackwardPropagation(output_layer,AFrelaxations,weights,biases,input_low,input_up)

% Loop on the layers, going backward
for l = output_layer:-1:0
    % Linear upper bound
    if l == output_layer
        Lambda{l} = eye(length(biases{l}));
        Delta{l} = zeros(length(biases{l}));        
        Omega{l} = eye(length(biases{l}));
        Theta{l} = zeros(length(biases{l}));    
    elseif l == 0
        for j = 1:size(Lambda{l+1},1)
            % Lambda matrix
            Lambda0(j,:) = (Lambda{l+1}(j,:)*weights{l+1}).*ones(1,size(weights{l+1},2));
            % Omega matrix
            Omega0(j,:) = (Omega{l+1}(j,:)*weights{l+1}).*ones(1,size(weights{l+1},2));
        end
    else
        for j = 1:size(Lambda{l+1},1)
            for i = 1:size(weights{l+1},2)
                % Upper bound
                % Lambda matrix
                if Lambda{l+1}(j,:)*weights{l+1}(:,i) >= 0
                    lambda{l}(j,i) = AFrelaxations(l,i,2,1);
                else
                    lambda{l}(j,i) = AFrelaxations(l,i,1,1);
                end
                % Delta matrix
                if Lambda{l+1}(j,:)*weights{l+1}(:,i) >= 0
                    Delta{l}(i,j) = AFrelaxations(l,i,2,2);
                else
                    Delta{l}(i,j) = AFrelaxations(l,i,1,2);
                end
                
                % Lower bound
                % Omega matrix
                if Omega{l+1}(j,:)*weights{l+1}(:,i) >= 0
                    omega{l}(j,i) = AFrelaxations(l,i,1,1);
                else
                    omega{l}(j,i) = AFrelaxations(l,i,2,1);
                end
                % Theta matrix
                if Omega{l+1}(j,:)*weights{l+1}(:,i) >= 0
                    Theta{l}(i,j) = AFrelaxations(l,i,1,2);
                else
                    Theta{l}(i,j) = AFrelaxations(l,i,2,2);
                end
            end
            % Lambda matrix
            Lambda{l}(j,:) = (Lambda{l+1}(j,:)*weights{l+1}).*lambda{l}(j,:);
            % Omega matrix
            Omega{l}(j,:) = (Omega{l+1}(j,:)*weights{l+1}).*omega{l}(j,:);
        end
    end
end

% Concretization of the pre-activation bounds for the last layer
% (only works if input bounds form an hypercube, and not an hyperrectangle)
x_low = NaN(size(weights{output_layer},1),1);
x_up = x_low;
input_nom = (input_low+input_up)/2;
input_rad = max((input_up-input_low)/2);
for i = 1:size(weights{output_layer},1)
    % Upper bound
    x_up(i) = input_rad*norm(Lambda0(i,:),1) + Lambda0(i,:)*input_nom;
    for l = 1:output_layer
        x_up(i) = x_up(i) + Lambda{l}(i,:)*(biases{l}+Delta{l}(:,i));
    end
    
    % Lower bound
    x_low(i) = -input_rad*norm(Omega0(i,:),1) + Omega0(i,:)*input_nom;
    for l = 1:output_layer
        x_low(i) = x_low(i) + Omega{l}(i,:)*(biases{l}+Theta{l}(:,i));
    end
end
end


