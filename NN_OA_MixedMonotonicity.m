%% Neural-network over-approximation using mixed-monotonicity

% At each layer
%   - create all partial networks up to this layer within the main network
%   - reachability analysis on each of these partial networks
%   - intersection of all results to be used as the next layer's input

% Inputs
%   weights: cell array of the (hidden_layers+1) weight matrices
%   biases: cell array of the (hidden_layers+1) bias vectors
%   Activation_function_choise: index of the chosen activation function
%         AF_IDENTITY = 1;
%         AF_SIGMOID = 3;
%         AF_TANH = 4;
%         AF_RELU = 5;
%         AF_PRELU = 6;
%         AF_SOFTPLUS = 7;
%         AF_ELU = 8;
%         AF_GELU = 9;        % Not monotone
%         AF_SILU = 10;       % Not monotone
%         AF_GAUSSIAN = 11;   % Not monotone
%   input_low: lower bound of the uncertainty range of the input vector
%   input_up: upper bound of the uncertainty range of the input vector

% Outputs
%   [x_low,x_up]: bounds on the output range of the network

function [x_low,x_up] = NN_OA_MixedMonotonicity(weights,biases,Activation_function_choise,input_low,input_up)
%% Extract activation function informations
[AF_handle,AF_local_bounds_handle,~,AF_derivative_local_bounds,~,~,~,~] = Activation_function_select(Activation_function_choise);

%% Initialization of the variable filled and used at each layer

% Cell array of the partial network function handles
Partial_network_descriptions = cell(length(weights),1);
for l = 1:length(weights)
    Partial_network_descriptions{l} = @(x) x;
end

% Cell arrays of the Jacobian bounds of each partial network
J_partial_low = cell(length(weights),1);
for l = 1:length(weights)
    J_partial_low{l} = eye(size(weights{l},2));
end
J_partial_up = J_partial_low;

% Cell arrays of the input bounds for each layer
Layer_input_low = cell(length(weights)+1,1);
Layer_input_up = cell(length(weights)+1,1);
Layer_input_low{1} = input_low;
Layer_input_up{1} = input_up;

% Loop on the network layers
for l = 1:length(weights)
    %% Initial description and analysis of the current layer
    
    % Initialization of the matrix to store all output bounds of this layer
    x_layer_low = NaN(length(biases{l}),l);
    x_layer_up = NaN(length(biases{l}),l);
    
    % Definition of the current layer
    System_description_layer = @(x) arrayfun(AF_handle,weights{l}*x + biases{l});
    
    % Bounds of this layer's affine transformation
    [affine_low,affine_up] = Interval_matrix_product(weights{l},weights{l},Layer_input_low{l},Layer_input_up{l});
    [affine_low,affine_up] = Interval_matrix_sum(affine_low,affine_up,biases{l},biases{l});

    % Local bounds of the activation function derivative for this layer
    AF_der_low = NaN(size(weights{l},1),1);
    AF_der_up = NaN(size(weights{l},1),1);
    for i = 1:size(weights{l},1)
        AF_der_bounds = AF_derivative_local_bounds(affine_low(i), affine_up(i));
        AF_der_low(i) = AF_der_bounds(1);
        AF_der_up(i) = AF_der_bounds(2);
    end

    %% Loop over all possible partial networks ending at this layer
    for k = 1:l
        % Define partial network based on previously saved networks
        Partial_network_descriptions{k} = @(x) System_description_layer(Partial_network_descriptions{k}(x));
        
        % Update Jacobian bounds of partial network
        [J_partial_low{k},J_partial_up{k}] = Interval_matrix_product(weights{l},weights{l},J_partial_low{k},J_partial_up{k});
        [J_partial_low{k},J_partial_up{k}] = Interval_matrix_product(diag(AF_der_low),diag(AF_der_up),J_partial_low{k},J_partial_up{k});

        % Mixed-monotone reachability analysis for the partial network
        [x_layer_low(:,k),x_layer_up(:,k)] = Algebraic_MM(Layer_input_low{k},Layer_input_up{k},J_partial_low{k},J_partial_up{k},Partial_network_descriptions{k});
    end
    
    %% Intersection of the layer's output bounds for all partial networks

    % The intersection of intervals is an interval with
    %   lower bound being the component-wise max of the lower bounds
    x_low = max(x_layer_low,[],2);
    %   upper bound being the component-wise min of the upper bounds
    x_up = min(x_layer_up,[],2);

    % Local bounds of the activation function for this layer
    AF_local_low = NaN(size(weights{l},1),1);
    AF_local_up = NaN(size(weights{l},1),1);
    for i = 1:size(weights{l},1)
        AF_local_bounds = AF_local_bounds_handle(affine_low(i), affine_up(i));
        AF_local_low(i) = AF_local_bounds(1);
        AF_local_up(i) = AF_local_bounds(2);
    end
    
    % Saturate the over-approximation to the activation function bounds
    x_low = max(x_low,AF_local_low);
    x_up = min(x_up,AF_local_up);

    % Save this layer's output bounds as next layer's input bounds
    Layer_input_low{l+1} = x_low;
    Layer_input_up{l+1} = x_up;
end

