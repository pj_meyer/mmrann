%% Neural-network over-approximation using naive interval bound propagation
% Generalization to non-monotone activation functions of the method
% presented in the paper: W. Xiang, H.-D. Tran, X. Yang, and T. T. Johnson.
% "Reachable set estimation for neural network control systems: A 
% simulation-guided approach". IEEE Transactions on Neural Networks and 
% Learning Systems, 32(5):1821–1830, 2020.

% Inputs
%   weights: cell array of the (hidden_layers+1) weight matrices
%   biases: cell array of the (hidden_layers+1) bias vectors
%   Activation_function_choise: index of the chosen activation function
%         AF_IDENTITY = 1;
%         AF_HEAVISIDE = 2;
%         AF_SIGMOID = 3;
%         AF_TANH = 4;
%         AF_RELU = 5;
%         AF_PRELU = 6;
%         AF_SOFTPLUS = 7;
%         AF_ELU = 8;
%         AF_GELU = 9;        % Not monotone
%         AF_SILU = 10;       % Not monotone
%         AF_GAUSSIAN = 11;   % Not monotone
%   input_low: lower bound of the uncertainty range of the input vector
%   input_up: upper bound of the uncertainty range of the input vector

% Outputs
%   [x_low,x_up]: bounds on the output range of the network

function [x_low,x_up] = NN_OA_IBP(weights,biases,Activation_function_choise,input_low,input_up)

%% Extract activation function informations
[~,AF_local_bounds,~,~,~,~,~] = Activation_function_select(Activation_function_choise);

%% Iterative propagation of the input intervals through the network
% using naive interval arithmetic operations

% Initialization to the input interval
x_low = input_low;
x_up = input_up;

% Propagate this interval through each layer of the network
for l = 1:length(weights)
    % Layer's affine transformation
    [x_low,x_up] = Interval_matrix_product(weights{l},weights{l},x_low,x_up);
    [x_low,x_up] = Interval_matrix_sum(x_low,x_up,biases{l},biases{l});
    
    % Layer's activation function
    for i = 1:length(x_low)
        % Propagate the interval through the AF for each neuron
        % since the function AF_local_bounds does not handle vector inputs
        AF_output = AF_local_bounds(x_low(i),x_up(i));
        x_low(i) = AF_output(1);
        x_up(i) = AF_output(2);
    end
end

