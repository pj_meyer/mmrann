%% Main script for the comparison on random NN for the LCSS/CDC22 paper
% For each of the selected activation functions, 
% generates a large number of random neural networks
% and calls and compares several reachability methods
% 1-IBP: Naive interval bound propagation (interval arithmetic)
% 2-ReluVal: symbolic interval propagation
% 3-Neurify: ReluVal+symbolic linear relaxation of ReLU
% 4-VeriNet: error-based symbolic error propagation
% 5-CROWN: backward propagation of linear bounds and AF relaxations
% 6-Mixed-monotonicity (MM): new method on all possible partial networks

% This script also:
% - generates one save file for each activation function
% - computes and displays the average time and width of the output bounds
% - displays how often the MM method outperforms the other 5
% - when applicable, display how often the methods fail to return a valid
%       output (Particularly for CROWN method with ELU activation)

close all
clear
datetime
p = gcp; % Parallel pool

%% Activation function index definitions
AF_IDENTITY = 1;
AF_HEAVISIDE = 2;   % Not continuous (so not applicable to the MM method)
AF_SIGMOID = 3;
AF_TANH = 4;
AF_RELU = 5;
AF_PRELU = 6;
AF_SOFTPLUS = 7;
AF_ELU = 8;
AF_GELU = 9;        % Not monotone
AF_SILU = 10;       % Not monotone
AF_GAUSSIAN = 11;   % Not monotone

%% Loop on the chosen activation functions
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Start of user-defined parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

for AF_choice=[AF_RELU,AF_TANH,AF_ELU,AF_SILU]
    %% Simulation choices
    
    % Number of generated NN
    n_simu = 10000;
    % Network parameter definitions (10000 smaller networks)
    n_input_min = 1;
    n_input_max = 10;
    n_output_min = 1;
    n_output_max = 10;
    hidden_layers_min = 0;   % (Actual number of computation layers is +1 with output layer)
    hidden_layers_max = 4;  % (Actual number of computation layers is +1 with output layer)
    n_node_min = 1;      % Min number of nodes per hidden layer
    n_node_max = 30;      % Max number of nodes per hidden layer


%     % Number of generated NN
%     n_simu = 1000;
%     % Network parameter definitions (1000 larger networks)
%     n_input_min = 500;
%     n_input_max = 1000;
%     n_output_min = 10;
%     n_output_max = 50;
%     hidden_layers_min = 4;   % (Actual number of computation layers is +1 with output layer)
%     hidden_layers_max = 9;  % (Actual number of computation layers is +1 with output layer)
%     n_node_min = 100;      % Min number of nodes per hidden layer
%     n_node_max = 200;      % Max number of nodes per hidden layer
    

    % Input bounds
    input_bounds_low = -1;
    input_bounds_up = 1;
    input_uncertainty = 0.1;
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % End of user-defined parameters
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    Activation_function_choise = AF_choice;
    
    %% Initialize save files
    n_methods = 6;
    n_layers_save = zeros(n_simu,1);
    n_nodes_save = zeros(n_simu,1);
    toc_save = zeros(n_simu,n_methods);
    x_low = zeros(n_output_max,n_simu,n_methods);
    x_up = zeros(n_output_max,n_simu,n_methods);

    %% Main loop for each network
    tic_full = tic;
    parfor nn = 1:n_simu
        %% Create random network
        n_input = randi([n_input_min n_input_max]);
        n_output = randi([n_output_min n_output_max]);
        hidden_layers = randi([hidden_layers_min hidden_layers_max]);

        [weights,biases] = Create_random_NN(n_input,n_output,hidden_layers,n_node_min,n_node_max);
        n_layers_save(nn) = numel(biases);
        for i=1:numel(biases)
            n_nodes_save(nn) = n_nodes_save(nn) + numel(biases{i});
        end

        %% Input bounds: fixed hypercube around a random point in [input_bounds_low,input_bouds_up]
        input_nom = input_bounds_low + (input_bounds_up-input_bounds_low)*rand(n_input,1);
        input_low = input_nom - input_uncertainty;
        input_up = input_nom + input_uncertainty;

        %% Temp variables
        temp_toc = zeros(1,n_methods);
        temp_low = zeros(n_output,n_methods);
        temp_up = zeros(n_output,n_methods);

        %% 1-IBP: Naive interval bound propagation (interval arithmetic)
        method = 1;
        tic
        [temp_low(:,method),temp_up(:,method)] = NN_OA_IBP(weights,biases,Activation_function_choise,input_low,input_up);
        temp_toc(method) = toc;

        %% 2-ReluVal: symbolic interval propagation
        if ismember(Activation_function_choise,[AF_IDENTITY,AF_HEAVISIDE,AF_RELU,AF_PRELU])
            method = 2;
            tic
            [temp_low(:,method),temp_up(:,method)] = NN_OA_ReluVal(weights,biases,Activation_function_choise,input_low,input_up);
            temp_toc(method) = toc;
        end

        %% 3-Neurify: ReluVal + symbolic linear relaxation of ReLU
        if ismember(Activation_function_choise,[AF_IDENTITY,AF_HEAVISIDE,AF_RELU])
            method = 3;
            tic
            [temp_low(:,method),temp_up(:,method)] = NN_OA_Neurify(weights,biases,Activation_function_choise,input_low,input_up);
            temp_toc(method) = toc;
        end

        %% 4-VeriNet: error-based symbolic error propagation
        if ismember(Activation_function_choise,[AF_IDENTITY,AF_HEAVISIDE,AF_SIGMOID,AF_TANH,AF_RELU,AF_SOFTPLUS,AF_ELU])
            method = 4;
            tic
            [temp_low(:,method),temp_up(:,method)] = NN_OA_Verinet(weights,biases,Activation_function_choise,input_low,input_up);
            temp_toc(method) = toc;
        end

        %% 5-CROWN: backward propagation of linear bounds and AF relaxations
        if ismember(Activation_function_choise,[AF_IDENTITY,AF_SIGMOID,AF_TANH,AF_RELU,AF_SOFTPLUS,AF_ELU])
            method = 5;
            tic
            [temp_low(:,method),temp_up(:,method)] = NN_OA_CROWN(weights,biases,Activation_function_choise,input_low,input_up);
            temp_toc(method) = toc;
        end

        %% 6-Mixed-monotonicity (MM): new method on all possible partial networks
        method = 6;
        tic
        [temp_low(:,method),temp_up(:,method)] = NN_OA_MixedMonotonicity(weights,biases,Activation_function_choise,input_low,input_up);
        temp_toc(method) = toc;

        %% Save sliced variables
        toc_save(nn,:) = temp_toc;
        temp_low = [temp_low;zeros(n_output_max-n_output,n_methods)];
        temp_up = [temp_up;zeros(n_output_max-n_output,n_methods)];
        x_low(:,nn,:) = temp_low;
        x_up(:,nn,:) = temp_up;

    end
    
    %% Create a save file
    filebase = ['./Saves/Bounding_Comparison_AF' num2str(AF_choice) '_Save'];
    file_index=1;
    while 1
        filename = [filebase num2str(file_index) '.mat'];
        % If the file does not exist, stop looking and compute the automaton
        if ~exist(filename,'file')
            break
        end
        file_index = file_index + 1;
    end
    save(filename);
    % The sections below can then be called after loading the save file
    
    %% Analysis and comparison of the results
    n_layers_save = repmat(n_layers_save,1,n_methods);
    n_nodes_save = repmat(n_nodes_save,1,n_methods);
    rounding_tol = 1e4*max(eps(vecnorm(x_up-x_low,2,1)),[],'all'); % Tolerance to consider as equal approximately equal results due to rounding errors

    % Time comparison
    time_mean(1,:) = mean(toc_save,1);
    time_diff(1,:) = sum((toc_save(:,1:5) - repmat(toc_save(:,6),1,5)) >= 0,1)*100/n_simu;

    % Time comparison per layer
    time_mean(2,:) = mean(toc_save./n_layers_save,1);
    time_diff(2,:) = sum((toc_save(:,1:5)./n_layers_save(:,1:5) - repmat(toc_save(:,6)./n_layers_save(:,6),1,5)) >= 0,1)*100/n_simu;

    % Time comparison per node
    time_mean(3,:) = mean(toc_save./n_nodes_save,1);
    time_diff(3,:) = sum((toc_save(:,1:5)./n_nodes_save(:,1:5) - repmat(toc_save(:,6)./n_nodes_save(:,6),1,5)) >= 0,1)*100/n_simu;

    % Width comparison
    width = shiftdim(vecnorm(x_up-x_low,2,1),1);
    width_mean(1,:) = mean(width,1,'omitnan');
    MM_tighter(1,:) = sum((width(:,1:5) - repmat(width(:,6),1,5)) >= rounding_tol,1)*100/n_simu;
    MM_looser(1,:) = sum((width(:,1:5) - repmat(width(:,6),1,5)) <= -rounding_tol,1)*100/n_simu;
    Approx_equal(1,:) = sum(abs(width(:,1:5) - repmat(width(:,6),1,5)) < rounding_tol,1)*100/n_simu;

    % Width comparison per layer
    width_mean(2,:) = mean(width./n_layers_save,1,'omitnan');
    MM_tighter(2,:) = sum((width(:,1:5)./n_layers_save(:,1:5) - repmat(width(:,6)./n_layers_save(:,6),1,5)) >= rounding_tol,1)*100/n_simu;
    MM_looser(2,:) = sum((width(:,1:5)./n_layers_save(:,1:5) - repmat(width(:,6)./n_layers_save(:,6),1,5)) <= -rounding_tol,1)*100/n_simu;
    Approx_equal(2,:) = sum(abs(width(:,1:5)./n_layers_save(:,1:5) - repmat(width(:,6)./n_layers_save(:,6),1,5)) < rounding_tol,1)*100/n_simu;

    % Width comparison per node
    width_mean(3,:) = mean(width./n_nodes_save,1,'omitnan');
    MM_tighter(3,:) = sum((width(:,1:5)./n_nodes_save(:,1:5) - repmat(width(:,6)./n_nodes_save(:,6),1,5)) >= rounding_tol,1)*100/n_simu;
    MM_looser(3,:) = sum((width(:,1:5)./n_nodes_save(:,1:5) - repmat(width(:,6)./n_nodes_save(:,6),1,5)) <= -rounding_tol,1)*100/n_simu;
    Approx_equal(3,:) = sum(abs(width(:,1:5)./n_nodes_save(:,1:5) - repmat(width(:,6)./n_nodes_save(:,6),1,5)) < rounding_tol,1)*100/n_simu;
    
    % NaN results (especially when CROWN cannot get horizontal relaxations with ELU)
    NaN_outputs = shiftdim(sum(any(isnan(x_low) | isnan(x_up),1),2)/n_simu*100);

    % Short name for all 6 methods
    Method_table = ["IBP";"ReluVal";"Neurify";"VeriNet";"CROWN";"MM"];

    % Choose type of results displayed (1=raw,2=wrt layer,3=wrt node)
    result_type = 3;

    % Diplay activation function
    switch Activation_function_choise
        case AF_IDENTITY
            fprintf('\nActivation function AF_IDENTITY (%d)',Activation_function_choise)
        case AF_HEAVISIDE
            fprintf('\nActivation function AF_HEAVISIDE (%d)',Activation_function_choise)
        case AF_SIGMOID
            fprintf('\nActivation function AF_SIGMOID (%d)',Activation_function_choise)
        case AF_TANH
            fprintf('\nActivation function AF_TANH (%d)',Activation_function_choise)
        case AF_RELU
            fprintf('\nActivation function AF_RELU (%d)',Activation_function_choise)
        case AF_PRELU
            fprintf('\nActivation function AF_PRELU (%d)',Activation_function_choise)
        case AF_SOFTPLUS
            fprintf('\nActivation function AF_SOFTPLUS (%d)',Activation_function_choise)
        case AF_ELU
            fprintf('\nActivation function AF_ELU (%d)',Activation_function_choise)
        case AF_GELU
            fprintf('\nActivation function AF_GELU (%d)',Activation_function_choise)
        case AF_SILU
            fprintf('\nActivation function AF_SILU (%d)',Activation_function_choise)
        case AF_GAUSSIAN
            fprintf('\nActivation function AF_GAUSSIAN (%d)',Activation_function_choise)
        otherwise
            fprintf('\nActivation function %d',Activation_function_choise)
    end
    
    % Display average time and output width
    fprintf('\nMethod\tMean time\tMean width\n');
    for i=1:n_methods
        fprintf('%s\t%f\t%f\n',Method_table(i),time_mean(result_type,i),width_mean(result_type,i))
    end
    
    % Display how often the MM method outperforms the other 5
    fprintf('\nPercentage comparison of the MM method against the other 5');
    fprintf('\nMethod\tMM faster\tMM looser\tEqual width\tMM tighter\tNaN output\n');
    for i=1:5
        fprintf('%s\t%f\t%f\t%f\t%f\t%f\n',Method_table(i),time_diff(result_type,i),MM_looser(result_type,i),Approx_equal(result_type,i),MM_tighter(result_type,i),NaN_outputs(i))
    end

    toc(tic_full)
end

datetime
