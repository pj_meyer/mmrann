%% Create a neural network with random parameters
% This only defines the weight and bias matrices for each layer
% which are not influenced by the activation function choices

% Inputs
%   n_input: number of input variables for the network
%   n_output: number of output variables for the network
%   hidden_layers: number of hidden layers
%   n_hidden_min: min number of nodes per hidden layer 
%   n_hidden_max: max number of nodes per hidden layer 
%       (actual number is picked randomly between n_hidden_min and n_hidden_max)

% Outputs
%   weights: cell array of the (hidden_layers+1) weight matrices
%   biases: cell array of the (hidden_layers+1) bias vectors

function [weights,biases] = Create_random_NN(n_input,n_output,hidden_layers,n_hidden_min,n_hidden_max)

% Random number of nodes per layer 
n_hidden = randi([n_hidden_min,n_hidden_max],hidden_layers,1);

% Dimensions of the inputs and outputs throughout the network
n_layers = [n_input;n_hidden;n_output];

% Initialization of the network parameters
weights = cell(hidden_layers+1,1);
biases = cell(hidden_layers+1,1);

% Loop on the network layers (hidden_layers hidden + 1 output layer)
for l = 1:(hidden_layers+1)
    % Random weight matrix taking values in [-1,1]
    weights{l} = -1 + 2*rand(n_layers(l+1),n_layers(l));     
    
    % Random bias vector taking values in [-1,1]
    biases{l} = -1 + 2*rand(n_layers(l+1),1); 
end
