%% Comparison of several reachability methods on the VNN benchmarks
% 1-IBP: Naive interval bound propagation (interval arithmetic)
% 2-ReluVal: symbolic interval propagation
% 3-Neurify: ReluVal+symbolic linear relaxation of ReLU
% 4-VeriNet: error-based symbolic error propagation
% 5-CROWN: backward propagation of linear bounds and AF relaxations
% 6-Mixed-monotonicity (MM): new method on all possible partial networks

% This script also:
% - generates one save file
% - computes and displays the average time and width of the output bounds
% - displays how often the MM method outperforms the other 5
% - when applicable, display how often the methods fail to return a valid
%       output (Particularly for CROWN method with ELU activation)

close all
clear

%% Activation function index definitions
AF_IDENTITY = 1;
AF_HEAVISIDE = 2;   % Not continuous (so not applicable to the MM method)
AF_SIGMOID = 3;
AF_TANH = 4;
AF_RELU = 5;
AF_PRELU = 6;
AF_SOFTPLUS = 7;
AF_ELU = 8;
AF_GELU = 9;        % Not monotone
AF_SILU = 10;       % Not monotone
AF_GAUSSIAN = 11;   % Not monotone

%% Benchmark index definitions
BM_ACAS = 1;        % VNN ACAS Xu benchmarks: run on all 45 networks, and take average
BM_ERAN_RELU = 2;   % VNN ERAN benchmark with ReLU, 8 hidden layers and 1 output layer
BM_ERAN_SIGMOID = 3;% VNN ERAN benchmark with Sigmoid, 6 hidden layers and 1 output layer
BM_MNISTFC_2 = 4;   % VNN MNISTFC benchmark with 2 hidden layers and 1 output layer
BM_MNISTFC_4 = 5;   % VNN MNISTFC benchmark with 4 hidden layers and 1 output layer
BM_MNISTFC_6 = 6;   % VNN MNISTFC benchmark with 6 hidden layers and 1 output layer
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Start of user-defined parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Benchmark index choice
BM_CHOICE = BM_ERAN_SIGMOID;

% Number of random MNIST picture used
%   for ACAS benchmark: automatically overwritten to 45
n_simu = 250;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% End of user-defined parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Read onnx file
switch BM_CHOICE
    case BM_ACAS
        onnx_file = 'ACASXU_run2a_1_1_batch_2000.onnx';
        n_simu = 45;
    case BM_ERAN_RELU
        onnx_file = 'mnist_relu_9_200.onnx';
    case BM_ERAN_SIGMOID
        onnx_file = 'ffnnSIGMOID__Point_6x200.onnx';
    case BM_MNISTFC_2
        onnx_file = 'mnist-net_256x2.onnx';
    case BM_MNISTFC_4
        onnx_file = 'mnist-net_256x4.onnx';
    case BM_MNISTFC_6
        onnx_file = 'mnist-net_256x6.onnx';
end

[weights,biases,AF_choice] = VNN_benchmark_extraction(onnx_file);
n_output = size(weights{end},1);

%% Input uncertainty
if BM_CHOICE == BM_ACAS
    % ACAS Xu: 45 benchmarks

    % Property 5
    input_low = [250;0.2;-pi;100;0];
    input_up = [400;0.4;-pi+0.005;400;400];
else
    % ERAN or MNIST-FC all use MNIST benchmark
    load('./Datasets/mnist.mat','XTest');

    % Define the benchmark-dependent input uncertainty
    switch BM_CHOICE
        case BM_ERAN_RELU
            input_uncertainty = 0.015;
        case BM_ERAN_SIGMOID
            input_uncertainty = 0.012;
%             input_uncertainty = 0.000012;
        case {BM_MNISTFC_2, BM_MNISTFC_4, BM_MNISTFC_6}
            input_uncertainty = 0.03;
    end
end

%% Initialize save files
n_methods = 6;
toc_save = zeros(n_simu,n_methods);
mnist_indices = zeros(n_simu,1);
x_low = zeros(n_output,n_simu,n_methods);
x_up = zeros(n_output,n_simu,n_methods);

%% Main loop for each network
tic_full = tic;
for nn = 1:n_simu
    %% Read new NN for ACAS benchmark
    if BM_CHOICE == BM_ACAS
        ACAS_param1 = floor((nn-1)/9)+1;
        ACAS_param2 = nn-(ACAS_param1-1)*9;
        onnx_file = ['ACASXU_run2a_' num2str(ACAS_param1) '_' num2str(ACAS_param2) '_batch_2000.onnx']
        [weights,biases,AF_choice] = VNN_benchmark_extraction(onnx_file);
    end
    
    %% Read new input_nom picture for benchmarks on MNIST
    if BM_CHOICE > BM_ACAS
        % Take a random MNIST image as nominal point
        mnist_indices(nn) = randi(size(XTest,4));
        fprintf('%s - Random MNIST image %d/%d: %d\n',datetime,nn,n_simu,mnist_indices(nn))
        input_nom = XTest(:,:,1,mnist_indices(nn));
        input_nom = double(input_nom(:));
    
        % Final input bounds: fixed hypercube around the nominal point
        input_low = input_nom - input_uncertainty;
        input_up = input_nom + input_uncertainty;
    end

    %% 1-IBP: Naive interval bound propagation (interval arithmetic)
    method = 1;
    tic
    [x_low(1:n_output,nn,method),x_up(1:n_output,nn,method)] = NN_OA_IBP(weights,biases,AF_choice,input_low,input_up);
    toc_save(nn,method) = toc;

    %% 2-ReluVal: symbolic interval propagation
    if ismember(AF_choice,[AF_IDENTITY,AF_HEAVISIDE,AF_RELU,AF_PRELU])
        method = 2;
        tic
        [x_low(1:n_output,nn,method),x_up(1:n_output,nn,method)] = NN_OA_ReluVal(weights,biases,AF_choice,input_low,input_up);
        toc_save(nn,method) = toc;
    end

    %% 3-Neurify: ReluVal + symbolic linear relaxation of ReLU
    if ismember(AF_choice,[AF_IDENTITY,AF_HEAVISIDE,AF_RELU])
        method = 3;
        tic
        [x_low(1:n_output,nn,method),x_up(1:n_output,nn,method)] = NN_OA_Neurify(weights,biases,AF_choice,input_low,input_up);
        toc_save(nn,method) = toc;
    end

    %% 4-VeriNet: error-based symbolic error propagation
    if ismember(AF_choice,[AF_IDENTITY,AF_HEAVISIDE,AF_SIGMOID,AF_TANH,AF_RELU,AF_SOFTPLUS,AF_ELU])
        method = 4;
        tic
        [x_low(1:n_output,nn,method),x_up(1:n_output,nn,method)] = NN_OA_Verinet(weights,biases,AF_choice,input_low,input_up);
        toc_save(nn,method) = toc;
    end

    %% 5-CROWN: backward propagation of linear bounds and AF relaxations
    if ismember(AF_choice,[AF_IDENTITY,AF_SIGMOID,AF_TANH,AF_RELU,AF_SOFTPLUS,AF_ELU])
        method = 5;
        tic
        [x_low(1:n_output,nn,method),x_up(1:n_output,nn,method)] = NN_OA_CROWN(weights,biases,AF_choice,input_low,input_up);
        toc_save(nn,method) = toc;
    end

    %% 6-Mixed-monotonicity (MM): new method on all possible partial networks
    method = 6;
    tic
    [x_low(1:n_output,nn,method),x_up(1:n_output,nn,method)] = NN_OA_MixedMonotonicity(weights,biases,AF_choice,input_low,input_up);
    toc_save(nn,method) = toc;
end

%% Create a save file
filebase = ['./Saves/VNN_benchmarks_' num2str(BM_CHOICE) '_Save'];
file_index=1;
while 1
    filename = [filebase num2str(file_index) '.mat'];
    % If the file does not exist, stop looking and compute the automaton
    if ~exist(filename,'file')
        break
    end
    file_index = file_index + 1;
end
save(filename,'BM_CHOICE','n_simu','toc_save','mnist_indices','x_low','x_up');
% The sections below can then be called after loading the save file

%% Analysis and comparison of the results
rounding_tol = 1e4*max(eps(vecnorm(x_up-x_low,2,1)),[],'all'); % Tolerance to consider as equal approximately equal results due to rounding errors

% Time comparison
time_mean(1,:) = mean(toc_save,1);
time_diff(1,:) = sum((toc_save(:,1:5) - repmat(toc_save(:,6),1,5)) >= 0,1)*100/n_simu;

% Width comparison
width = shiftdim(vecnorm(x_up-x_low,2,1),1);
width_mean(1,:) = mean(width,1,'omitnan');
% width_diff(1,:) = sum((width(:,1:5) - repmat(width(:,6),1,5)) >= -rounding_tol,1)*100/n_simu;
MM_tighter(1,:) = sum((width(:,1:5) - repmat(width(:,6),1,5)) >= rounding_tol,1)*100/n_simu;
MM_looser(1,:) = sum((width(:,1:5) - repmat(width(:,6),1,5)) <= -rounding_tol,1)*100/n_simu;
Approx_equal(1,:) = sum(abs(width(:,1:5) - repmat(width(:,6),1,5)) < rounding_tol,1)*100/n_simu;

% NaN results (especially when VeriNet and CROWN cannot get horizontal relaxations)
NaN_outputs = shiftdim(sum(any(isnan(x_low) | isnan(x_up),1),2)/n_simu*100);

% Short name for all 6 methods
Method_table = ["IBP";"ReluVal";"Neurify";"VeriNet";"CROWN";"MM"];

% Choose type of results displayed (1=raw,2=wrt layer,3=wrt node)
result_type = 1;

% Diplay activation function
switch AF_choice
    case AF_IDENTITY
        fprintf('\nActivation function AF_IDENTITY (%d)',AF_choice)
    case AF_HEAVISIDE
        fprintf('\nActivation function AF_HEAVISIDE (%d)',AF_choice)
    case AF_SIGMOID
        fprintf('\nActivation function AF_SIGMOID (%d)',AF_choice)
    case AF_TANH
        fprintf('\nActivation function AF_TANH (%d)',AF_choice)
    case AF_RELU
        fprintf('\nActivation function AF_RELU (%d)',AF_choice)
    case AF_PRELU
        fprintf('\nActivation function AF_PRELU (%d)',AF_choice)
    case AF_SOFTPLUS
        fprintf('\nActivation function AF_SOFTPLUS (%d)',AF_choice)
    case AF_ELU
        fprintf('\nActivation function AF_ELU (%d)',AF_choice)
    case AF_GELU
        fprintf('\nActivation function AF_GELU (%d)',AF_choice)
    case AF_SILU
        fprintf('\nActivation function AF_SILU (%d)',AF_choice)
    case AF_GAUSSIAN
        fprintf('\nActivation function AF_GAUSSIAN (%d)',AF_choice)
    otherwise
        fprintf('\nActivation function %d',AF_choice)
end

% Display average time and output width
fprintf('\nMethod\tMean time\tMean width\n');
for i=1:n_methods
    fprintf('%s\t%f\t%f\n',Method_table(i),time_mean(result_type,i),width_mean(result_type,i))
end

% Display how often the MM method outperforms the other 5
fprintf('\nPercentage comparison of the MM method against the other 5');
fprintf('\nMethod\tMM faster\tMM looser\tEqual width\tMM tighter\tNaN output\n');
for i=1:5
    fprintf('%s\t%f\t%f\t%f\t%f\t%f\n',Method_table(i),time_diff(result_type,i),MM_looser(result_type,i),Approx_equal(result_type,i),MM_tighter(result_type,i),NaN_outputs(i))
end

toc(tic_full)
