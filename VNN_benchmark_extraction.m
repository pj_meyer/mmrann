%% VNN benchmark extraction
% into variables in the format usable by my reachability methods

function [weights,biases,AF_choice] = VNN_benchmark_extraction(onnx_file)

%% Activation function index definitions
AF_IDENTITY = 1;
AF_HEAVISIDE = 2;   % Not continuous (so not applicable to the MM method)
AF_SIGMOID = 3;
AF_TANH = 4;
AF_RELU = 5;
AF_PRELU = 6;
AF_SOFTPLUS = 7;
AF_ELU = 8;
AF_GELU = 9;        % Not monotone
AF_SILU = 10;       % Not monotone
AF_GAUSSIAN = 11;   % Not monotone

%% Import ONNX network
onnx_folder = './Datasets/VNN_benchmarks/';
matlab_function = importONNXFunction([onnx_folder onnx_file],'output_function_name');

%% Extract NN parameters
if contains(onnx_file,'ACAS')
    % ACAS Xu from VNN (45 networks available)
    % onnx_file = 'ACASXU_run2a_<x>_<y>_batch_2000.onnx'; x in 1-5, y in 1-9
    weights{1} = extractdata(matlab_function.Learnables.Operation_1_MatMul_W);
    weights{2} = extractdata(matlab_function.Learnables.Operation_2_MatMul_W);
    weights{3} = extractdata(matlab_function.Learnables.Operation_3_MatMul_W);
    weights{4} = extractdata(matlab_function.Learnables.Operation_4_MatMul_W);
    weights{5} = extractdata(matlab_function.Learnables.Operation_5_MatMul_W);
    weights{6} = extractdata(matlab_function.Learnables.Operation_6_MatMul_W);
    weights{7} = extractdata(matlab_function.Learnables.linear_7_MatMul_W);
    biases{1} = extractdata(matlab_function.Nonlearnables.Operation_1_Add_B);
    biases{2} = extractdata(matlab_function.Nonlearnables.Operation_2_Add_B);
    biases{3} = extractdata(matlab_function.Nonlearnables.Operation_3_Add_B);
    biases{4} = extractdata(matlab_function.Nonlearnables.Operation_4_Add_B);
    biases{5} = extractdata(matlab_function.Nonlearnables.Operation_5_Add_B);
    biases{6} = extractdata(matlab_function.Nonlearnables.Operation_6_Add_B);
    biases{7} = extractdata(matlab_function.Nonlearnables.linear_7_Add_B);

elseif contains(onnx_file,'mnist_relu_9_200') || contains(onnx_file,'ffnnSIGMOID__Point_6x200')
    % ERAN MNIST from VNN
    % onnx_file = 'mnist_relu_9_200.onnx'; % ReLU 9 layers
    % onnx_file = 'ffnnSIGMOID__Point_6x200.onnx'; % Sigmoid 7 layers
    weights{1} = extractdata(matlab_function.Learnables.x2_weight)';
    weights{2} = extractdata(matlab_function.Learnables.x4_weight)';
    weights{3} = extractdata(matlab_function.Learnables.x6_weight)';
    weights{4} = extractdata(matlab_function.Learnables.x8_weight)';
    weights{5} = extractdata(matlab_function.Learnables.x10_weight)';
    weights{6} = extractdata(matlab_function.Learnables.x12_weight)';
    weights{7} = extractdata(matlab_function.Learnables.x14_weight)';
    biases{1} = extractdata(matlab_function.Learnables.x2_bias);
    biases{2} = extractdata(matlab_function.Learnables.x4_bias);
    biases{3} = extractdata(matlab_function.Learnables.x6_bias);
    biases{4} = extractdata(matlab_function.Learnables.x8_bias);
    biases{5} = extractdata(matlab_function.Learnables.x10_bias);
    biases{6} = extractdata(matlab_function.Learnables.x12_bias);
    biases{7} = extractdata(matlab_function.Learnables.x14_bias);
    if contains(onnx_file,'mnist_relu_9_200')
        weights{8} = extractdata(matlab_function.Learnables.x16_weight)';
        weights{9} = extractdata(matlab_function.Learnables.x18_weight)';
        biases{8} = extractdata(matlab_function.Learnables.x16_bias);
        biases{9} = extractdata(matlab_function.Learnables.x18_bias);
    end

elseif contains(onnx_file,'mnist-net')
    % MNIST-FC from VNN 
    % onnx_file = 'mnist-net_256x2.onnx'; % 3 layers
    % onnx_file = 'mnist-net_256x4.onnx'; % 5 layers
    % onnx_file = 'mnist-net_256x6.onnx'; % 7 layers
    weights{1} = extractdata(matlab_function.Learnables.layers_0_weight)';
    weights{2} = extractdata(matlab_function.Learnables.layers_2_weight)';
    weights{3} = extractdata(matlab_function.Learnables.layers_4_weight)';
    biases{1} = extractdata(matlab_function.Learnables.layers_0_bias);
    biases{2} = extractdata(matlab_function.Learnables.layers_2_bias);
    biases{3} = extractdata(matlab_function.Learnables.layers_4_bias);
    if contains(onnx_file,'mnist-net_256x4') || contains(onnx_file,'mnist-net_256x6')
        weights{4} = extractdata(matlab_function.Learnables.layers_6_weight)';
        weights{5} = extractdata(matlab_function.Learnables.layers_8_weight)';
        biases{4} = extractdata(matlab_function.Learnables.layers_6_bias);
        biases{5} = extractdata(matlab_function.Learnables.layers_8_bias);
        if contains(onnx_file,'mnist-net_256x6')
            weights{6} = extractdata(matlab_function.Learnables.layers_10_weight)';
            weights{7} = extractdata(matlab_function.Learnables.layers_12_weight)';
            biases{6} = extractdata(matlab_function.Learnables.layers_10_bias);
            biases{7} = extractdata(matlab_function.Learnables.layers_12_bias);
        end
    end

else
    error('Input file not recognized among the VNN benchmarks')
end

%% Set the choice of the activation function

% Most of them are using ReLU
AF_choice = AF_RELU;

% Apart from one of the ERAN MNIST benchmarks
if contains(onnx_file,'SIGMOID')
    AF_choice = AF_SIGMOID;    
end

%% Delete created matlab function
if exist('output_function_name.m','file')
    delete output_function_name.m
end
