%% Neural-network over-approximation using symbolic interval propagation
% From Neurify paper: Shiqi Wang, Kexin Pei, Justin Whitehouse, Junfeng 
% Yang, and Suman Jana. "Efficient Formal Safety Analysis of Neural 
% Networks". 32nd Conference on Neural Information Processing Systems
% (NIPS) 2018

% This approach is restricted to piecewise-linear activation functions
% (Identity, Heaviside, ReLU)

% This method builds on top of the ReluVal method (implemented in function 
% NN_OA_ReluVal.m) with symbolic interval propagation.
% The novelty is that unlike ReluVal that concretizes the symbolic
% equations when the bounds contains the nonlinearity of the ReLU function,
% here we use symbolic linear relaxations to keep the input dependencies.

% Inputs
%   weights: cell array of the (hidden_layers+1) weight matrices
%   biases: cell array of the (hidden_layers+1) bias vectors
%   Activation_function_choise: index of the chosen activation function
%   input_low: lower bound of the uncertainty range of the input vector
%   input_up: upper bound of the uncertainty range of the input vector

% Outputs
%   [x_low,x_up]: bounds on the output range of the network

function [x_low,x_up] = NN_OA_Neurify(weights,biases,Activation_function_choise,input_low,input_up)
n_input = length(input_low);
max_layer_size = 0;
for i=1:length(biases)
    max_layer_size = max(max_layer_size,length(biases{i}));
end

%% Activation function index definitions
AF_IDENTITY = 1;
AF_HEAVISIDE = 2;
AF_RELU = 5;
% AF_PRELU = 6; % currently not implemented

%% Extract activation function informations
assert(ismember(Activation_function_choise,[AF_IDENTITY,AF_HEAVISIDE,AF_RELU]),'Activation function not supported by the method with symbolic intervals');
[AF_handle,AF_local_bounds,~,~,~,~,~,~] = Activation_function_select(Activation_function_choise);

%% Iterative propagation of the input intervals through the network
% using symbolic intervals and symbolic linear relaxations

% Matrix of the symbolic equation coefficients for each node of a layer
LB_symbolic_coeff = zeros(max_layer_size,n_input+1);
UB_symbolic_coeff = zeros(max_layer_size,n_input+1);

% Initialization of the symbolic equations (equal to the input vector)
LB_symbolic_coeff(1:n_input,1:n_input) = eye(n_input);
UB_symbolic_coeff(1:n_input,1:n_input) = eye(n_input);

% Initialize output of the network
x_low = zeros(length(biases{end}),1);
x_up = zeros(length(biases{end}),1);

% Propagate the symbolic interval through each layer of the network
for l = 1:length(weights)
    %% Layer's affine transformation   
    new_LB_symbolic_coeff = zeros(max_layer_size,n_input+1);
    new_UB_symbolic_coeff = zeros(max_layer_size,n_input+1);
    
    % Loop on output nodes of this layer
    for i = 1:size(weights{l},1)
        LB_eq_eval_low = 0;
        LB_eq_eval_up = 0;
        UB_eq_eval_low = 0;
        UB_eq_eval_up = 0;
        
        % Loop on input nodes of this layer
        for j = 1:size(weights{l},2)
            % Loop on the inputs of the network (+the constant coefficient)
            for k = 1:length(input_low)+1
                % Update equation coefficients after linear transformation
                if weights{l}(i,j) >= 0
                    new_LB_symbolic_coeff(i,k) = new_LB_symbolic_coeff(i,k) + LB_symbolic_coeff(j,k)*weights{l}(i,j);
                    new_UB_symbolic_coeff(i,k) = new_UB_symbolic_coeff(i,k) + UB_symbolic_coeff(j,k)*weights{l}(i,j);
                else
                    new_LB_symbolic_coeff(i,k) = new_LB_symbolic_coeff(i,k) + UB_symbolic_coeff(j,k)*weights{l}(i,j);
                    new_UB_symbolic_coeff(i,k) = new_UB_symbolic_coeff(i,k) + LB_symbolic_coeff(j,k)*weights{l}(i,j);
                end 
            end
        end
        
        % Loop on the inputs of the network
        for k = 1:length(input_low)
            % Update evaluation of the equations
            if new_LB_symbolic_coeff(i,k) >= 0
                LB_eq_eval_low = LB_eq_eval_low + new_LB_symbolic_coeff(i,k)*input_low(k);
                LB_eq_eval_up = LB_eq_eval_up + new_LB_symbolic_coeff(i,k)*input_up(k);
            else
                LB_eq_eval_low = LB_eq_eval_low + new_LB_symbolic_coeff(i,k)*input_up(k);
                LB_eq_eval_up = LB_eq_eval_up + new_LB_symbolic_coeff(i,k)*input_low(k);
            end
            if new_UB_symbolic_coeff(i,k) >= 0
                UB_eq_eval_low = UB_eq_eval_low + new_UB_symbolic_coeff(i,k)*input_low(k);
                UB_eq_eval_up = UB_eq_eval_up + new_UB_symbolic_coeff(i,k)*input_up(k);
            else
                UB_eq_eval_low = UB_eq_eval_low + new_UB_symbolic_coeff(i,k)*input_up(k);
                UB_eq_eval_up = UB_eq_eval_up + new_UB_symbolic_coeff(i,k)*input_low(k);
            end
        end
        
        % Add bias
        new_LB_symbolic_coeff(i,end) = new_LB_symbolic_coeff(i,end) + biases{l}(i);
        new_UB_symbolic_coeff(i,end) = new_UB_symbolic_coeff(i,end) + biases{l}(i);
        LB_eq_eval_low = LB_eq_eval_low + new_LB_symbolic_coeff(i,end);
        LB_eq_eval_up = LB_eq_eval_up + new_LB_symbolic_coeff(i,end);
        UB_eq_eval_low = UB_eq_eval_low + new_UB_symbolic_coeff(i,end);
        UB_eq_eval_up = UB_eq_eval_up + new_UB_symbolic_coeff(i,end);

        %% Layer's activation function (and relaxation if required)
        if l < length(weights)
            switch Activation_function_choise
                case AF_IDENTITY
                    % No nonlinearity in this AF
                    % So we can always simply propagate both symbolic equations

                case AF_HEAVISIDE
                    % Degenerate case where symbolic equations are never propagated
                    % No matter the range values, we concretize them to 0 or 1
                    new_LB_symbolic_coeff(i,:) = 0;
                    new_UB_symbolic_coeff(i,:) = 0;

                    % Lower-bound equation
                    %   Concretize to 1 if the range is fully non-negative
                    %   Concretize to 0 otherwise
                    if LB_eq_eval_low >= 0
                        new_LB_symbolic_coeff(i,end) = 1;
                    end

                    % Upper-bound equation
                    %   Concretize to 1 if the range has some positive values
                    %   Concretize to 0 otherwise
                    if UB_eq_eval_up > 0
                        new_UB_symbolic_coeff(i,end) = 1;
                    end

                case AF_RELU
                    % Lower-bound equation
                    %   Propagate symbolic equation if the range is fully positive
                    if LB_eq_eval_up <= 0
                        % Concretize equation to 0 if the range is fully negative
                        new_LB_symbolic_coeff(i,:) = 0;
                    elseif LB_eq_eval_low < 0
                        % Symbolic linear relaxation otherwise
                        new_LB_symbolic_coeff(i,:) = new_LB_symbolic_coeff(i,:)*LB_eq_eval_up/(LB_eq_eval_up-LB_eq_eval_low);
                    end

                    % Upper-bound equation
                    %   Propagate symbolic equation if the range is fully positive
                    if UB_eq_eval_up <= 0
                        % Concretize equation to 0 if the range is fully negative
                        new_UB_symbolic_coeff(i,:) = 0;
                    elseif UB_eq_eval_low < 0
                        % Symbolic linear relaxation otherwise
                        new_UB_symbolic_coeff(i,:) = new_UB_symbolic_coeff(i,:)*UB_eq_eval_up/(UB_eq_eval_up-UB_eq_eval_low);
                        new_UB_symbolic_coeff(i,end) = new_UB_symbolic_coeff(i,end) - UB_eq_eval_low*UB_eq_eval_up/(UB_eq_eval_up-UB_eq_eval_low);
                    end

            end
        else
            % If we are at the last layer: 
            % simply apply the activation function to the concretized ranges
            
            % Propagate the interval through the AF for each neuron
            % since the function AF_local_bounds does not handle vector inputs
            AF_output = AF_local_bounds(LB_eq_eval_low,UB_eq_eval_up);
            x_low(i) = AF_output(1);
            x_up(i) = AF_output(2);
        end
        
    end
    
    % Save coefficients for next layer's computations
    LB_symbolic_coeff = new_LB_symbolic_coeff;
    UB_symbolic_coeff = new_UB_symbolic_coeff;

end
