# MMRANN: Mixed-Monotonicity Reachability Analysis of Neural Networks

This page provides the code and benchmarks used to obtain the numerical results presented in the paper [Reachability analysis of neural networks using mixed monotonicity](https://arxiv.org/pdf/2111.07683.pdf) published in IEEE Control Systems Letters.

The paper presents a novel method based on mixed monotonicity for the reachability analysis of feedforward neural networks, and applicable to networks with any Lipschitz-continuous activation function.
The numerical section compares this new approach with the bounding methods from 5 others tools from the literature (Interval Bound Propagation, ReluVal, Neurify, VeriNet, CROWN). The comparisons are done on benchmarks from the [2021 VNN competition](https://github.com/stanleybak/vnncomp2021), as well as on large sets of randomly generated neural networks of various width and depth.

## Content of the folder

* Folder `Datasets` contains 4 benchmarks from the 2021 VNN competition (3 for MNIST-FC with ReLU activation, and the ERAN-sigmoid benchmark) and the MNIST dataset used for testing these benchmarks.
* Folder `Saves` contains the mat-files corresponding to the results presented in numerical comparisons of Section IV of the paper.
* Bounding methods
	* Function `NN_OA_MixedMonotonicity.m` is the novel reachability analysis approach presented in the paper.
	* Functions `NN_OA_IBP.m`, `NN_OA_ReluVal.m`, `NN_OA_Verinet.m`, `NN_OA_Neurify.m`, `NN_OA_CROWN.m` are a personal re-implementation of the bounding methods of these other neural network verification tools from the literature. Note that these implementation are primarily based on the described methods in each original publication (see references in the paper), and may thus differ slightly from the latest updates of their public toolboxes.
* Main scripts
	* File `NN_OA_comparison_random_NN.m` compares these 6 bounding methods on a set of randomly generated neural networks.
	* File `NN_OA_comparison_VNN_benchmarks.m` compares these 6 bounding methods on benchmarks from the folder `Datasets`.
* Utility functions
	* Function `Activation_function_select.m` allows users to define new activation functions to be used in the main comparison scripts (see Section II.C of the paper).
	* Function `Algebraic_MM.m` is the mixed-monotonicity reachability analysis called several times by the algorithm `NN_OA_MixedMonotonicity.m`.
	* Function `Create_random_NN.m` generates random weight matrices and bias vectors of a neural network according to the requested dimensions.
	* Functions `Interval_matrix_product.m` and `Interval_matrix_sum.m` are basic interval arithmetic operators.
	* Function `VNN_benchmark_extraction.m` extracts the weight matrices and bias vectors of the ONNX benchmark files.

## Citing this work

IEEE L-CSS-2022 paper [**Reachability analysis of neural networks using mixed monotonicity**](https://arxiv.org/pdf/2111.07683.pdf)
```
@article{meyer2022lcss,
  title={Reachability analysis of neural networks using mixed monotonicity},
  author={Meyer, Pierre-Jean},
  journal={IEEE Control Systems Letters},
  year={2022},
  volume={6},
  pages={3068-3073},
  doi={10.1109/LCSYS.2022.3182547}
}
```

## Contributor

* [**Pierre-Jean Meyer**](http://chapal.eu/pierre-jean_meyer/), COSYS-ESTAS, Univ Gustave Eiffel, Lille
	* email @univ-eiffel.fr : pierre-jean.meyer

## License

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.

