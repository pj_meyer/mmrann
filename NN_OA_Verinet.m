%% Neural-network over-approximation using symbolic interval propagation
% From VeriNet paper: P. Henriksen and A. Lomuscio, "Efﬁcient Neural 
% Network Veriﬁcation via Adaptive Reﬁnement and Adversarial Search". ECAI 
% 2020

% This approach is restricted to piecewise-linear activation functions
% (Identity, Heaviside, ReLU) and S-shape functions (tanh, sigmoid).
% Two convex functions (SoftPlus, ELU) that are not handled by the original
% version of VeriNet are added to this implementation.

% This method is similar to the Neurify method (implemented in function 
% NN_OA_Neurify.m) but with a slightly different approach when computing 
% the symbolic equations.
% Instead of computing 2 symbolic equations bounding the outputs of each
% layer as in the Neurify function, they obtain two linear bounds from
% computing a single symbolic equation alongside an error matrix, which are
% both propagated through the layers.
% This alternative method is called ESIP: Error-based Symbolic Interval
% Propagation.

% Inputs
%   weights: cell array of the (hidden_layers+1) weight matrices
%   biases: cell array of the (hidden_layers+1) bias vectors
%   Activation_function_choise: index of the chosen activation function
%   input_low: lower bound of the uncertainty range of the input vector
%   input_up: upper bound of the uncertainty range of the input vector

% Outputs
%   [x_low,x_up]: bounds on the output range of the network

function [x_low,x_up] = NN_OA_Verinet(weights,biases,Activation_function_choise,input_low,input_up)
n_input = length(input_low);
max_layer_size = 0;
for i=1:length(biases)
    max_layer_size = max(max_layer_size,length(biases{i}));
end

% Number of iterations to find near-optimal relaxations of S-shaped AF
n_iter = 2;

%% Activation function index definitions
AF_IDENTITY = 1;
AF_HEAVISIDE = 2;
AF_SIGMOID = 3;
AF_TANH = 4;
AF_RELU = 5;
AF_SOFTPLUS = 7;
AF_ELU = 8;

%% Extract activation function informations
assert(ismember(Activation_function_choise,[AF_IDENTITY,AF_HEAVISIDE,AF_SIGMOID,AF_TANH,AF_RELU,AF_SOFTPLUS,AF_ELU]),'Activation function not supported by the method with symbolic intervals');
[AF_handle,AF_local_bounds,AF_derivative,~,~,~,~,~] = Activation_function_select(Activation_function_choise);

%% Iterative propagation of the input intervals through the network
% using symbolic intervals and symbolic linear relaxations

% Matrix of the symbolic equation coefficients for each node of a layer
Symbolic_coeff = zeros(max_layer_size,n_input+1);

% Initialization of the symbolic equation (equal to the input vector)
Symbolic_coeff(1:n_input,1:n_input) = eye(n_input);

% Initialize output of the network
x_low = zeros(length(biases{end}),1);
x_up = zeros(length(biases{end}),1);

% Initialization of the error matrix
Error_matrix = zeros(n_input,0);

% Propagate the symbolic interval through each layer of the network
for l = 1:length(weights)
    %% Layer's affine transformation   
    
    % Update equation coefficients after linear transformation
    Symbolic_coeff(1:size(weights{l},1),:) = weights{l}*Symbolic_coeff(1:size(weights{l},2),:);
    Symbolic_coeff(1:size(weights{l},1),end) = Symbolic_coeff(1:size(weights{l},1),end) + biases{l};
    
    % Update error matrix after linear transformation
    Error_matrix = weights{l}*Error_matrix;

    % Loop on output nodes of this layer
    for i = 1:size(weights{l},1)
        %% Concretization of the pre-activation bounds
        Concretization_low = 0;
        Concretization_up = 0;
        
        % Loop on the inputs of the network
        for k = 1:length(input_low)
            % Update concretization of the equation
            if Symbolic_coeff(i,k) >= 0
                Concretization_low = Concretization_low + Symbolic_coeff(i,k)*input_low(k);
                Concretization_up = Concretization_up + Symbolic_coeff(i,k)*input_up(k);
            else
                Concretization_low = Concretization_low + Symbolic_coeff(i,k)*input_up(k);
                Concretization_up = Concretization_up + Symbolic_coeff(i,k)*input_low(k);
            end
        end
        
        % Add bias
        Concretization_low = Concretization_low + Symbolic_coeff(i,end);
        Concretization_up = Concretization_up + Symbolic_coeff(i,end);
        
        % Add negative error terms to lower bound 
        % and positive ones to upper bound
        if size(Error_matrix,2)>0
            Concretization_low = Concretization_low + sum(min(Error_matrix(i,:),0));
            Concretization_up = Concretization_up + sum(max(Error_matrix(i,:),0));
        end

        %% Linear relaxations of the activation function
        % for the current node i, based on its concretization bounds
        slope_low = 0;
        offset_low = 0;
        slope_up = 0;
        offset_up = 0;
        
        if Concretization_up == Concretization_low
            % Skip if bounds are equal
            offset_low = AF_handle(Concretization_up);
            offset_up = offset_low;
        else
            % Otherwise, the relaxations depends on the activation type
            switch Activation_function_choise
                case AF_IDENTITY
                    % No nonlinearity in this AF, so no need for relaxation
                    slope_low = 1;
                    slope_up = 1;

                case AF_HEAVISIDE
                    % Degenerate case where relaxations are horizontal
                    if Concretization_low >= 0
                        offset_low = 1;
                        offset_up = 1;
                    elseif Concretization_up > 0
                        offset_low = 0;
                        offset_up = 1;
                    end

                case AF_SIGMOID
                    % Preliminary computations
                    intercept_slope = (AF_handle(Concretization_up)-AF_handle(Concretization_low))/(Concretization_up-Concretization_low);
                    intercept_offset = AF_handle(Concretization_up) - intercept_slope*Concretization_up;
                    tangent_slope = AF_derivative((Concretization_low+Concretization_up)/2);
                    tangent_offset = AF_handle((Concretization_low+Concretization_up)/2) - tangent_slope*(Concretization_low+Concretization_up)/2;
                    
                    % Lower relaxation
                    if intercept_slope <= AF_derivative(Concretization_low)
                        slope_low = intercept_slope;
                        offset_low = intercept_offset;
                    elseif (tangent_slope*Concretization_up+tangent_offset) <= AF_handle(Concretization_up)
                        slope_low = tangent_slope;
                        offset_low = tangent_offset;
                    else
                        x_iter = Concretization_low;
                        for optim_iterations = 1:n_iter
                            temp = sqrt(1-4*(AF_handle(x_iter)-AF_handle(Concretization_up))/(x_iter-Concretization_up))/2;
                            x_iter = -log(1/(0.5-temp)-1);
                        end
                        slope_low = AF_derivative(x_iter);
                        offset_low = AF_handle(x_iter) - slope_low*x_iter;
                    end
                    
                    % Upper relaxation
                    if intercept_slope <= AF_derivative(Concretization_up)
                        slope_up = intercept_slope;
                        offset_up = intercept_offset;
                    elseif (tangent_slope*Concretization_low+tangent_offset) >= AF_handle(Concretization_low)
                        slope_up = tangent_slope;
                        offset_up = tangent_offset;
                    else
                        x_iter = Concretization_up;
                        for optim_iterations = 1:n_iter
                            temp = sqrt(1-4*(AF_handle(x_iter)-AF_handle(Concretization_low))/(x_iter-Concretization_low))/2;
                            x_iter = -log(1/(0.5+temp)-1);
                        end
                        slope_up = AF_derivative(x_iter);
                        offset_up = AF_handle(x_iter) - slope_up*x_iter;
                    end

                case AF_TANH
                    % Preliminary computations
                    intercept_slope = (AF_handle(Concretization_up)-AF_handle(Concretization_low))/(Concretization_up-Concretization_low);
                    intercept_offset = AF_handle(Concretization_up) - intercept_slope*Concretization_up;
                    tangent_slope = AF_derivative((Concretization_low+Concretization_up)/2);
                    tangent_offset = AF_handle((Concretization_low+Concretization_up)/2) - tangent_slope*(Concretization_low+Concretization_up)/2;
                    
                    % Lower relaxation
                    if intercept_slope <= AF_derivative(Concretization_low)
                        slope_low = intercept_slope;
                        offset_low = intercept_offset;
                    elseif (tangent_slope*Concretization_up+tangent_offset) <= AF_handle(Concretization_up)
                        slope_low = tangent_slope;
                        offset_low = tangent_offset;
                    else
                        x_iter = Concretization_low;
                        for optim_iterations = 1:n_iter
                            temp = sqrt(1-(AF_handle(x_iter)-AF_handle(Concretization_up))/(x_iter-Concretization_up));
                            x_iter = log((1-temp)/(1+temp))/2;
                        end
                        slope_low = AF_derivative(x_iter);
                        offset_low = AF_handle(x_iter) - slope_low*x_iter;
                    end
                    
                    % Upper relaxation
                    if intercept_slope <= AF_derivative(Concretization_up)
                        slope_up = intercept_slope;
                        offset_up = intercept_offset;
                    elseif (tangent_slope*Concretization_low+tangent_offset) >= AF_handle(Concretization_low)
                        slope_up = tangent_slope;
                        offset_up = tangent_offset;
                    else
                        x_iter = Concretization_up;
                        for optim_iterations = 1:n_iter
                            temp = sqrt(1-(AF_handle(x_iter)-AF_handle(Concretization_low))/(x_iter-Concretization_low));
                            x_iter = log((1+temp)/(1-temp))/2;
                        end
                        slope_up = AF_derivative(x_iter);
                        offset_up = AF_handle(x_iter) - slope_up*x_iter;
                    end
                    
                case AF_RELU
                    % Lower-bound equation
                    if Concretization_low >= 0
                        slope_low = 1;
                        slope_up = 1;
                    elseif Concretization_up > 0
                        slope_low = Concretization_up/(Concretization_up-Concretization_low);
                        slope_up = slope_low;
                        offset_up = -slope_low*Concretization_low;
                    end  
                    
                case AF_SOFTPLUS
                    % Lower relaxation
                    slope_low = AF_derivative((Concretization_low+Concretization_up)/2);
                    offset_low = AF_handle((Concretization_low+Concretization_up)/2) - slope_low*(Concretization_low+Concretization_up)/2;
                    % Upper relaxation
                    slope_up = (AF_handle(Concretization_up)-AF_handle(Concretization_low))/(Concretization_up-Concretization_low);
                    offset_up = AF_handle(Concretization_up) - slope_up*Concretization_up;

                case AF_ELU
                    % Lower relaxation
                    slope_low = AF_derivative((Concretization_low+Concretization_up)/2);
                    offset_low = AF_handle((Concretization_low+Concretization_up)/2) - slope_low*(Concretization_low+Concretization_up)/2;
                    % Upper relaxation
                    slope_up = (AF_handle(Concretization_up)-AF_handle(Concretization_low))/(Concretization_up-Concretization_low);
                    offset_up = AF_handle(Concretization_up) - slope_up*Concretization_up;

            end
        end
        
        %% Propagation of the equation through the relaxations
        Symbolic_coeff(i,:) = Symbolic_coeff(i,:)*slope_low;
        Symbolic_coeff(i,end) = Symbolic_coeff(i,end) + offset_low;
        
        %% Propagation of the error matrix through the relaxaxtions
        if size(Error_matrix,2)>0
            Error_matrix(i,:) = Error_matrix(i,:)*slope_low;
        end
        error_low = Concretization_low*(slope_up-slope_low) + offset_up - offset_low;
        error_up = Concretization_up*(slope_up-slope_low) + offset_up - offset_low;
        if max(error_low,error_up)>0
            Error_matrix(i,end+1) = max(error_low,error_up);
        end

        %% Get concrete output bounds if we are at the last layer
        if l == length(weights)
            % Propagate the interval through the AF for each neuron
            % since the function AF_local_bounds does not handle vector inputs
            AF_output = AF_local_bounds(Concretization_low,Concretization_up);
            x_low(i) = AF_output(1);
            x_up(i) = AF_output(2);
        end
        
    end
end
